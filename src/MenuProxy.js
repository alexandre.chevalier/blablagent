import Cheerio from 'cheerio'
import axios from 'axios'

const url = 'https://cors-anywhere.herokuapp.com/http://www.laboucle-cantine.com/cantine'

export async function getMenuFor(day) {
  let menu
  let definitiveUrl = url
  if (day) definitiveUrl = `${url}?jourdelasemaine=${day}`
  await axios
    .get(definitiveUrl)
    .then(response => Promise.resolve(response.data))
    .then(html => {
      const $ = Cheerio.load(html)
      function getFood(products, labelType) {
        return products
          .filter((index, element) => (
            $(element)
              .find('.product_category_title')
              .text()
              .toLowerCase()
              .includes(labelType)
          ))
          .map((index, element) => $(element)
            .find('.product_titre')
            .text())
          .get()
      }
      const produits = $('.produit-grid')
      menu = {
        starters: getFood(produits, 'entrée'),
        dishes: getFood(produits, 'plat'),
        desserts: getFood(produits, 'dessert'),
      }
    })
  return menu
}

export const menu = {
  lundi: getMenuFor('lundi'),
  mardi: getMenuFor('mardi'),
  mercredi: getMenuFor('mercredi'),
  jeudi: getMenuFor('jeudi'),
  vendredi: getMenuFor('vendredi'),
}
