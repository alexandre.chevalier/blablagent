import React from 'react'
import AgentDevis from './components/AgentDevis'

export default () => (
  <div>
    <h1>Devis</h1>
    <p className="centered">Demandez-lui donc un devis.</p>
    <AgentDevis />
  </div>
)
