import React from 'react'
import AgentDemo from './components/AgentDemo'
import AgentCommande from './components/AgentCommande'

const button = {
  backgroundColor: '#4CAF50',
  border: 'none',
  color: 'white',
  padding: '12px 28px',
  textAlign: 'center',
  textDecoration: 'none',
  fontSize: '18px',
  display: 'flex',
  margin: 'auto',
  marginTop: '30px',
  boxShadow: '0 12px 24px 0 rgba(0,0,0,0.15)',
  borderRadius: '12px',
  fontFamily: 'Roboto',
  outline: '0',
}

export default class extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      demo: true,
    }
    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState({ demo: !this.state.demo })
  }

  render() {
    if (this.state.demo) {
      return (
        <div>
          <h1>Démo</h1>
          <AgentDemo />
          <button style={button} onClick={this.toggle}>Commande</button>
        </div>
      )
    }
    return (
      <div>
        <h1>Démo</h1>
        <AgentCommande />
        <button style={button} onClick={this.toggle}>Menu</button>
      </div>
    )
  }
}
