import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#2c3e50',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#2c3e50',
  botFontColor: '#fff',
}

const steps = [
  {
    id: '1',
    message: 'Bienvenue !',
    trigger: '2',
  },
  {
    id: '2',
    message: 'En quoi puis-je vous aider ?',
    user: true,
    trigger: '3',
  },
  {
    id: '3',
    message: 'Très bien.',
    end: true,
  },
]

export default class AgentWelcome extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 100)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="welcome-bot"
            headerTitle={'Agent blabla'}
            botDelay={500}
            userDelay={0}
            steps={steps}
          />
        </ThemeProvider>
      )
    } return (
      <div className="centered">
        <MDSpinner size={60} />
      </div>
    )
  }
}
