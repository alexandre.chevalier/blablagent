import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const Infos = props => {
  const { steps } = props
  const nom = steps.nom.value
  const mail = steps.mail.value.toLowerCase()
  const message = steps.message ? steps.message.value : ''
  const quoteStyle = {
    fontSize: '24px',
    fontFamily: 'Times New Roman',
  }
  return (
    <div className="infos">
      <div>Résumé de votre demande de contact
         au nom de <b>{nom}</b> ({mail}) :</div>
      <p>Votre message :</p>
      <div>
        <span style={quoteStyle}>&#8220;</span>
        {message}
        <span style={quoteStyle}>&#8221;</span>
      </div>
    </div>
  )
}

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#2c3e50',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#2c3e50',
  botFontColor: '#fff',
}

export default class AgentContact extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 600)
  }

  render() {
    function notBlank(value) {
      if (value.trim().length === 0) {
        return 'Le texte ne doit pas être vide.'
      }
      return true
    }
    function validateMmail(email) {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (!re.test(email)) {
        return "L'adresse mail est invalide."
      }
      return true
    }

    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="contact-bot"
            hideHeader
            width={'500px'}
            botDelay={300}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={[
              {
                id: '1',
                message: 'Prenez contact avec nous !',
                trigger: '2',
              },
              {
                id: '2',
                message: 'Comment vous appelez-vous ?',
                trigger: 'nom',
              },
              {
                id: 'nom',
                user: true,
                validator: value => notBlank(value),
                trigger: '5',
              },
              {
                id: '5',
                message: 'Quel est votre adresse mail ?',
                trigger: 'mail',
              },
              {
                id: 'mail',
                user: true,
                validator: value => validateMmail(value),
                trigger: '6',
              },
              {
                id: '6',
                message: 'Laissez-nous un petit message.',
                trigger: 'message',
              },
              {
                id: 'message',
                user: true,
                trigger: '7',
              },
              {
                id: '7',
                component: <Infos />,
                trigger: '8',
                // asMessage: true,
              },
              {
                id: '8',
                options: [
                  { value: 1, label: "C'est tout bon !", trigger: '9' },
                  { value: 0, label: 'Recommencer', trigger: '2' },
                ],
              },
              {
                id: '9',
                message: "Merci d'avoir pris contact.",
                trigger: '10',
              },
              {
                id: '10',
                message: 'Nous vous répondrons au plus vite, à bientôt !',
                end: true,
              },
            ]}
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={80} />
      </div>
    )
  }
}

