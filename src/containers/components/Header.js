import React from 'react'
import { Router, Link } from 'react-static'

export default class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      checked: false,
    }

    this.uncheck = this.uncheck.bind(this)
    this.toggle = this.toggle.bind(this)
  }

  uncheck() {
    this.setState({ checked: false })
  }

  toggle() {
    this.setState({ checked: !this.state.checked })
  }

  render() {
    return (
      <Router>
        <div className="nav">
          <div className="nav-header">
            <div className="nav-title">
              <Link className="logo-link" to="/">
                <img
                  className=""
                  src="/logo.png"
                  alt="( blablagent )"
                  width="140"
                />
              </Link>
            </div>
          </div>
          <div className="nav-btn">
            <label htmlFor="nav-check">
              <span />
              <span />
              <span />
            </label>
          </div>
          <input type="checkbox" id="nav-check" onClick={this.toggle} checked={this.state.checked} />
          <div className="nav-links">
            <Link className="header-link" to="/offres" onClick={this.uncheck}>Offres</Link>
            <Link className="header-link" to="/demo" onClick={this.uncheck}>Démo</Link>
            <Link className="header-link" to="/devis" onClick={this.uncheck}>Devis</Link>
            <Link className="header-link" to="/contact" onClick={this.uncheck}>Contact</Link>
          </div>
        </div>
      </Router>
    )
  }
}
