import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#2c3e50',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#2c3e50',
  botFontColor: '#fff',
}

export default class AgentDevis extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 600)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="home-bot"
            headerTitle={'Agent devis'}
            width={'500px'}
            botDelay={300}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={[
              {
                id: '1',
                message: "C'est parti pour le devis !",
                trigger: '2',
              },
              {
                id: '2',
                message: 'De quoi avez-vous besoin ?',
                trigger: 'besoin',
              },
              {
                id: 'besoin',
                options: [
                  { value: 0, label: 'Un truc basique', trigger: 'basique_0' },
                  { value: 1, label: 'Un truc avancé', trigger: 'avance_0' },
                ],
              },
              {
                id: 'basique_0',
                message: 'Un truc basique du genre...',
                trigger: 'basique_1',
              },
              {
                id: 'basique_1',
                options: [
                  { value: 0, label: 'Aide à la visite', trigger: 'basique_visite' },
                  { value: 1, label: 'F.A.Q', trigger: 'basique_faq' },
                ],
              },
              {
                id: 'basique_visite',
                message: 'Pour une aide à la visite, l\'offre basic devrait vous suffire !',
                trigger: 'try_again',
              },
              {
                id: 'basique_faq',
                message: 'Seulement pour les questions les plus souvent posées ?',
                trigger: 'basique_faq_souvent',
              },
              {
                id: 'basique_faq_souvent',
                options: [
                  { value: 0, label: 'Oui', trigger: 'basique_faq_souvent_oui' },
                  { value: 1, label: 'Non', trigger: 'basique_faq_souvent_non' },
                ],
              },
              {
                id: 'basique_faq_souvent_oui',
                message: 'Alors une offre basic répondra à votre besoin !',
                trigger: 'try_again',
              },
              {
                id: 'basique_faq_souvent_non',
                message: 'Dans ce cas, l\'offre smart devrait vous aller !',
                trigger: 'try_again',
              },
              {
                id: 'avance_0',
                message: 'Un truc avancé du genre...',
                trigger: 'avance_1',
              },
              {
                id: 'avance_1',
                options: [
                  { value: 0, label: 'Support client', trigger: 'avance_support' },
                  { value: 1, label: 'Spécifique', trigger: 'avance_specifique' },
                ],
              },
              {
                id: 'avance_support',
                message: 'Dans ce cas, l\'offre smart devrait vous aller !',
                trigger: 'try_again',
              },
              {
                id: 'avance_specifique',
                message: 'Alors il faudra voir l\'offre genius !',
                trigger: 'try_again',
              },
              {
                id: 'try_again',
                message: 'Voulez-vous un autre devis ?',
                trigger: 'try_again_ask',
              },
              {
                id: 'try_again_ask',
                options: [
                  { value: 0, label: 'Recommencer', trigger: '1' },
                ],
              },
            ]}
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={80} />
      </div>
    )
  }
}

