import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#e67e00',
  headerFontColor: '#fff',
  headerFontSize: '18px',
  botBubbleColor: '#e67e00',
  botFontColor: '#fff',
  // userBubbleColor: '#fff',
  // userFontColor: '#4a4a4a',
}

export default class AgentBla extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 600)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="bla-bot"
            headerTitle={'Basic'}
            width={'320px'}
            botDelay={600}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={[
              {
                id: '1',
                message: 'Salut !',
                trigger: '2',
              },
              {
                id: '2',
                message: 'Je suis un robot scripté',
                trigger: '3',
              },
              {
                id: '3',
                message: 'Je ne suis pas le plus intelligent',
                trigger: '4',
              },
              {
                id: '4',
                message: 'Je fais des réponses simples',
                trigger: '5',
              },
              {
                id: '5',
                message: 'Mais je suis très utile !',
                trigger: '6',
              },
              {
                id: '6',
                message: 'Je suis disponible à partir de 49€/mois !',
                end: true,
              },
            ]}
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={80} singleColor="#e67e00" />
      </div>
    )
  }
}

