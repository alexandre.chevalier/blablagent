import React from 'react'
import axios from 'axios'
import moment from 'moment'
import Menu from './Menu'

const url = 'https://api.dialogflow.com/v1/query'
const bearer = 'Bearer 9e83b06260c848b3932db3503a202fcf'

moment.locale('fr')

class DialogFlow extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      date: '',
      action: '',
      trigger: false,
    }

    this.handleResponse = this.handleResponse.bind(this)
    this.handleError = this.handleError.bind(this)
    this.request = this.request.bind(this)
    this.triggerNext = this.triggerNext.bind(this)
  }

  componentWillMount() {
    const { steps } = this.props
    const search = steps.userRequest.value
    this.request(search)
  }

  handleResponse(serverResponse) {
    const textResult = serverResponse.result.fulfillment.speech
    const dateResult = serverResponse.result.parameters['date-time']
    const actionResult = serverResponse.result.action
    this.setState({
      action: actionResult,
      text: textResult,
      date: dateResult,
    })
    this.triggerNext()
  }

  handleError(serverError) {
    console.log(serverError)
    this.setState({
      result: 'Erreur ! Veuillez me redémarrer.',
    })
  }

  request(text) {
    axios
      .get(url, {
        params: {
          v: '20150910',
          lang: 'fr',
          sessionId: 'dev-session-id',
          query: text,
        },
        headers: {
          Authorization: bearer,
        },
      })
      .then(response => response.data)
      .then(this.handleResponse)
      .catch(this.handleError)
  }

  triggerNext() {
    this.setState({ trigger: true }, () => {
      this.props.triggerNextStep()
    })
  }

  render() {
    if (this.state.action === 'menu') {
      let day = moment().format('dddd')
      if (moment(this.state.date).isValid()) {
        day = moment(this.state.date).format('dddd')
      }

      if (day === 'samedi' || day === 'dimanche') {
        return "Il n'y a pas de menu le week-end !"
      }

      const dateString = this.state.date ?
        `(${moment(this.state.date).format('L')})` : ''
      return (
        <div>
          {`${this.state.text} ${dateString} :`}
          <br />
          <br />
          <Menu day={day} />
        </div>
      )
    }
    if (this.state.action !== 'menu') {
      return <div>{this.state.text}</div>
    }
  }
}

export default DialogFlow
