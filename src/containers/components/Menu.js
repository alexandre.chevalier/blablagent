import React from 'react'
import shortid from 'shortid'
import { menu } from './StaticMenu'

export default class Menu extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      starters: [],
      dishes: [],
      desserts: [],
    }
  }

  componentWillMount() {
    this.setState(menu[this.props.day])
  }

  render() {
    return (
      <div>
        En entrée :
        <ul>{this.state.starters.map(name => <li key={shortid.generate()}>{name}</li>)}</ul>
        En plat :
        <ul>{this.state.dishes.map(name => <li key={shortid.generate()}>{name}</li>)}</ul>
        En dessert :
        <ul>{this.state.desserts.map(name => <li key={shortid.generate()}>{name}</li>)}</ul>
      </div>
    )
  }
}
