import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

import DialogFlow from './DialogFlow'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#000',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#000',
  botFontColor: '#fff',
  // userBubbleColor: '#fff',
  // userFontColor: '#4a4a4a',
}


export default class AgentDemo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      fork: false,
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 600)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="home-bot"
            headerTitle={'Agent de la boucle'}
            width={'500px'}
            botDelay={500}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={
              [{
                id: 'welcome',
                message: 'Bienvenue !',
                trigger: 'ask',
              },
              {
                id: 'ask',
                message: 'Demande-moi quelque chose.',
                trigger: 'userRequest',
              },
              {
                id: 'userRequest',
                user: true,
                validator: value => {
                  if (value.trim().length === 0) {
                    return 'Le texte ne doit pas être vide.'
                  }
                  if (value.includes('commande')) {
                    this.setState({ fork: true })
                    console.log(this.state)
                  }
                  return true
                },
                trigger: 'apiAnswer',
              },
              {
                id: 'apiAnswer',
                component: <DialogFlow />,
                delay: 500,
                waitAction: true,
                asMessage: true,
                trigger: 'userRequest',
              },
              ]
            }
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={60} />
      </div>
    )
  }
}

