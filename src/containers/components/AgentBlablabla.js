import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#000',
  headerFontColor: '#fff',
  headerFontSize: '18px',
  botBubbleColor: '#000',
  botFontColor: '#fff',
  // userBubbleColor: '#fff',
  // userFontColor: '#4a4a4a',
}

export default class AgentBlablabla extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 600)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="bla-bot"
            headerTitle={'Genius'}
            width={'320px'}
            botDelay={400}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={[
              {
                id: '1',
                message: 'Bienvenue !',
                trigger: '2',
              },
              {
                id: '2',
                message: 'Je suis le plus intelligent !',
                trigger: '3',
              },
              {
                id: '3',
                message: 'Je comprends certaines phrases',
                trigger: '4',
              },
              {
                id: '4',
                message: 'Et je vous réponds de façon ciblée',
                trigger: '5',
              },
              {
                id: '5',
                message: 'Je suis même capable d\'apprendre !',
                trigger: '6',
              },
              {
                id: '6',
                message: 'Je suis disponible à partir de 199€/mois !',
                end: true,
              },
            ]}
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={80} singleColor="#000" />
      </div>
    )
  }
}

