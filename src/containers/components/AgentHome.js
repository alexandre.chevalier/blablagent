import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#2c3e50',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#2c3e50',
  botFontColor: '#fff',
}

const steps = [
  {
    id: '1',
    message: 'Bienvenue !',
    trigger: '2',
  },
  {
    id: '2',
    message: 'En quoi puis-je vous aider ?',
    trigger: '3',
  },
  {
    id: '3',
    message: 'Choisissez un sujet',
    trigger: '4',
  },
  {
    id: '4',
    options: [
      { value: 1, label: 'blablagent', trigger: 'blablagent' },
      { value: 2, label: 'Offres', trigger: 'offres' },
      { value: 3, label: 'Démo', trigger: 'demo' },
      { value: 4, label: 'Devis', trigger: 'devis' },
    ],
  },
  {
    id: 'blablagent',
    message: 'blablagent est une super chouette entreprise développant des agents conversationnels.',
    trigger: '3',
  },
  {
    id: 'offres',
    message: 'Nous proposons 3 offres de base. Cliquez sur l\'onglet offres pour plus de détails !',
    trigger: '3',
  },
  {
    id: 'demo',
    message: "Une petite démo vous attend en cliquant sur l'onglet Démo !",
    trigger: '3',
  },
  {
    id: 'devis',
    message: 'Nous pouvons vous faire un devis gratuit en ligne. Cliquez sur l\'onglet devis pour y accéder !',
    trigger: '3',
  },
]


export default class AgentHome extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 100)
  }

  render() {
    if (this.state.loaded) {
      const inputStyle = {
        display: 'none',
      }
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="home-bot"
            headerTitle={'Agent blabla'}
            floating
            botDelay={500}
            userDelay={0}
            steps={steps}
            inputStyle={inputStyle}
          />
        </ThemeProvider>
      )
    } return (
      <div className="centered">
        <MDSpinner size={60} />
      </div>
    )
  }
}

