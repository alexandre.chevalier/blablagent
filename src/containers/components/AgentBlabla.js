import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#2980b9',
  headerFontColor: '#fff',
  headerFontSize: '18px',
  botBubbleColor: '#2980b9',
  botFontColor: '#fff',
  // userBubbleColor: '#fff',
  // userFontColor: '#4a4a4a',
}

export default class AgentBlabla extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 600)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="bla-bot"
            headerTitle={'Smart'}
            width={'320px'}
            botDelay={500}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={[
              {
                id: '1',
                message: 'Bonjour !',
                trigger: '2',
              },
              {
                id: '2',
                message: 'Je suis un robot intelligent',
                trigger: '3',
              },
              {
                id: '3',
                message: 'J\'essaye de vous comprendre',
                trigger: '4',
              },
              {
                id: '4',
                message: 'Et je vous réponds au mieux',
                trigger: '5',
              },
              {
                id: '5',
                message: 'Je suis prêt à vous aider !',
                trigger: '6',
              },
              {
                id: '6',
                message: 'Je suis disponible à partir de 99€/mois !',
                end: true,
              },
            ]}
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={80} singleColor="#2980b9" />
      </div>
    )
  }
}

