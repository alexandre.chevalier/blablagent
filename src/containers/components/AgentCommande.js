import React from 'react'
import { ThemeProvider } from 'styled-components'
import ChatBot from 'react-simple-chatbot'
import MDSpinner from 'react-md-spinner'

const Total = props => {
  const { steps } = props
  const entree = steps.entree.value
  const plat = steps.plat.value
  const dessert = steps.dessert.value
  const boisson = steps.boisson.value

  let e = 'Non'
  let p = 'Non'
  let d = 'Non'
  let b = 'Non'

  if (entree === 1) {
    e = 'Salade de quinoa'
  }
  if (plat === 1) {
    p = 'Cabillaud vapeur'
  }
  if (plat === 2) {
    p = 'Sauté de porc'
  }
  if (dessert === 1) {
    d = 'Fraîcheur ananas'
  }
  if (dessert === 2) {
    d = 'Muffin'
  }
  if (boisson === 1) {
    b = 'Oui'
  }

  let result = 0
  // Entrée, plat et dessert
  if (entree === 1 && plat > 0 && dessert > 0) {
    result = 11.20
  }
  // Entrée et plat
  if (entree === 1 && plat > 0 && dessert === 0) {
    result = 9.50
  }
  // Plat et dessert
  if (entree === 0 && plat > 0 && dessert > 0) {
    result = 9.50
  }
  // Un plat
  if (entree === 0 && dessert === 0 && plat > 0) {
    result = 7.50
  }
  // Un dessert
  if (entree === 0 && dessert > 0 && plat === 0) {
    result = 2.50
  }
  // Une entrée
  if (entree === 1 && dessert === 0 && plat === 0) {
    result = 2.50
  }

  if (boisson === 1) {
    result += 2
  }

  return (
    <div className="total">
      Résumé de votre commande :
      <ul>
        <li>Entrée : {e}</li>
        <li>Plat : {p}</li>
        <li>Dessert : {d}</li>
        <li>Boisson : {b}</li>
      </ul>
      Total : {result} €
    </div>
  )
}

const theme = {
  background: '#f5f8fb',
  fontFamily: 'Roboto',
  headerBgColor: '#000',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#000',
  botFontColor: '#fff',
  // userBubbleColor: '#fff',
  // userFontColor: '#4a4a4a',
}

export default class AgentCommande extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      fork: false,
      loaded: false,
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loaded: true })
    }, 400)
  }

  render() {
    if (this.state.loaded) {
      return (
        <ThemeProvider theme={theme} >
          <ChatBot
            className="home-bot"
            headerTitle={'Commande'}
            width={'500px'}
            botDelay={500}
            userDelay={0}
            placeholder={"Ecrire à l'agent..."}
            steps={
              [{
                id: 'commande',
                message: 'Très bien ! Commençons.',
                trigger: 'c_1',
              },
              {
                id: 'c_1',
                message: 'Souhaitez-vous une entrée ?',
                trigger: 'c_2',
              },
              {
                id: 'c_2',
                message: '- Salade de quinoa, betteraves rouges, feta et noisettes concassées',
                trigger: 'entree',
              },
              {
                id: 'entree',
                options: [
                  { value: 1, label: 'Oui', trigger: 'p_1' },
                  { value: 0, label: 'Non merci', trigger: 'p_1' },
                  { value: 9, label: 'Annuler', trigger: 'cancel' },
                ],
              },
              {
                id: 'p_1',
                message: 'Désirez-vous un plat ?',
                trigger: 'p_2',
              },
              {
                id: 'p_2',
                message: '- Cabillaud vapeur citron curry, fondue de poireaux à la crème et riz basmati',
                trigger: 'p_3',
              },
              {
                id: 'p_3',
                message: '- Sauté de porc laqué au miel, zeste d’oranges et gingembre et boulgour façon cantonais',
                trigger: 'plat',
              },
              {
                id: 'plat',
                options: [
                  { value: 1, label: 'Cabillaud vapeur', trigger: 'd_1' },
                  { value: 2, label: 'Sauté de porc', trigger: 'd_1' },
                  { value: 0, label: 'Non merci', trigger: 'd_1' },
                  { value: 9, label: 'Annuler', trigger: 'cancel' },
                ],
              },
              {
                id: 'd_1',
                message: 'Désirez-vous un dessert ?',
                trigger: 'd_2',
              },
              {
                id: 'd_2',
                message: '- Fraicheur ananas, mangue, kiwi chantilly aux fruits de la passion',
                trigger: 'd_3',
              },
              {
                id: 'd_3',
                message: '- Muffin pommes cannelle',
                trigger: 'dessert',
              },
              {
                id: 'dessert',
                options: [
                  { value: 1, label: 'Fraicheur ananas', trigger: 'b_1' },
                  { value: 2, label: 'Muffin', trigger: 'b_1' },
                  { value: 0, label: 'Non merci', trigger: 'b_1' },
                  { value: 9, label: 'Annuler', trigger: 'cancel' },
                ],
              },
              {
                id: 'b_1',
                message: 'Une boisson pour vous rafraîchir ?',
                trigger: 'boisson',
              },
              {
                id: 'boisson',
                options: [
                  { value: 1, label: 'Oui', trigger: 'total' },
                  { value: 0, label: 'Non', trigger: 'total' },
                  { value: 9, label: 'Annuler', trigger: 'cancel' },
                ],
              },
              {
                id: 'total',
                component: <Total />,
                asMessage: true,
                trigger: 'end',
              },
              {
                id: 'end',
                message: 'Merci d\'avoir commandé !',
                end: true,
              },
              {
                id: 'cancel',
                message: 'Très bien, au revoir !',
                end: true,
              },
              ]
            }
          />
        </ThemeProvider>
      )
    }
    return (
      <div className="centered">
        <MDSpinner size={60} />
      </div>
    )
  }
}

