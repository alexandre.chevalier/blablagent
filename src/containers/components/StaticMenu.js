export const menu = {
  lundi: {
    starters:
    ['Velouté de panais'],
    dishes:
    ['Filet mignon de porc au cheddar et bacon, crème à la Savora, écrasé de pommes de terre vapeur et haricots verts', 'Gratin de ravioles, gorgonzola et courgettes au pesto'],
    desserts:
    ['Tiramisu crème de marrons et chocolat', 'Compote pommes, poires et kiwis'],
  },
  mardi: {
    starters:
    ['Flan d’épinards, chèvre cendrée et coulis de tomates'],
    dishes:
    ['Filet de poulet, crème de courge butternut, risotto potirons, champignons de paris', 'Lasagnes fondue de poireaux, saumon et dos de colin'],
    desserts:
    ['Tiramisu crème de marrons et chocolat', 'Bavarois vanille, coulis fruits rouges'],
  },
  mercredi: {
    starters:
    ['Salade de lentilles vertes et saumon fumé'],
    dishes:
    ['Chili con carne et quinoa au curcuma', 'Cuisse de poulet rôti au four, pommes de terre au thym, tomate provençale et petits pois vapeur'],
    desserts:
    ['Verrine d’agrumes au miel, fromage blanc et Spéculoos', 'Muffin pommes cannelle'],
  },
  jeudi: {
    starters:
    ['Crumble salé à la noisette, panais, potirons et carottes'],
    dishes:
    ['Mijotée de veau à la bière, légumes d’automne et pâtes', 'Tajine de poulet, citron, menthe, semoule, épices douces et petits légumes'],
    desserts:
    ['Brownie', 'Muffin pommes cannelle'],
  },
  vendredi: {
    starters:
    ['Salade de quinoa, betteraves rouges, feta et noisettes concassées'],
    dishes:
    ['Cabillaud vapeur citron curry, fondue de poireaux à la crème et riz basmati', 'Sauté de porc laqué au miel, zeste d’oranges et gingembre et boulgour façon cantonais'],
    desserts:
    ['Fraicheur ananas, mangue, kiwi chantilly aux fruits de la passion', 'Muffin pommes cannelle'],
  },
}
