import React from 'react'
import AgentContact from './components/AgentContact'

export default () => (
  <div>
    <h1>Contact</h1>
    <AgentContact />
  </div>

)
