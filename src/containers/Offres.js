import React from 'react'
import AgentBla from './components/AgentBla'
import AgentBlabla from './components/AgentBlabla'
import AgentBlablabla from './components/AgentBlablabla'

const containerStyle = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  flexWrap: 'wrap',
  paddingTop: '10px',
}

const marginStyle = {
  marginLeft: '1em',
  marginRight: '1em',
  marginBottom: '1em',
}

export default () => (
  <div>
    <h1>Offres</h1>
    <div style={containerStyle}>
      <div style={marginStyle}><AgentBla /></div>
      <div style={marginStyle}><AgentBlabla /></div>
      <div style={marginStyle}><AgentBlablabla /></div>
    </div>
  </div>
)
