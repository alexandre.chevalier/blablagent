import React from 'react'
import AgentHome from './components/AgentHome'
import AgentBla from './components/AgentBla'
import AgentBlabla from './components/AgentBlabla'
import AgentBlablabla from './components/AgentBlablabla'

const paragraph = {
  textAlign: 'center',
  paddingLeft: '10px',
  paddingRight: '10px',
}

export default () => (
  <div>
    <h1>Nous créons des agents conversationnels.</h1>
    <p className="home-paragraph" style={paragraph}>
      Pour vous rendre service et vous simplifier la vie.
    </p>
    <figure className="icon-cards">
      <div className="icon-cards__content">
        <div className="icon-cards__item"><AgentBla /></div>
        <div className="icon-cards__item"><AgentBlabla /></div>
        <div className="icon-cards__item"><AgentBlablabla /></div>
      </div>
    </figure>
    <AgentHome />
  </div >
)

// { /* <div style={p}>
//     <h1>Nous créons des agents conversationnels.</h1>
//     <h2 className="centered">
//       Pour vous rendre service et vous simplifier la vie.
//     </h2>
//     <div className="centered">
//       <img style={img} alt="( blablagent )" className="centered" src="logo.png" width="50%" />
//       <h2>Dites bonjour au futur, dites bonjour à blablagent.</h2>
//     </div>
//     <p style={p}>
//       Nous avons toujours rêvé de communiquer avec un robot.
//       Un outil de discussion intelligent, qui nous comprenne et qui nous réponde.
//       Un robot si malin qu'il s'efface au profit de la seule expérience utilisateur.
//       Un agent de conversation qui réponde à la moindre de nos interogations, 
//       de nos attentes, de nos désirs.
//       C’est chose faite aujourd’hui.
//     </p> */ }
