import axios from 'axios'
import React, { Component } from 'react'

export default {
  getSiteProps: () => ({
    title: 'le site du robot',
  }),
  getRoutes: async() => {
    const { data: posts } = await axios.get('https://jsonplaceholder.typicode.com/posts')
    return [
      {
        path: '/',
        component: 'src/containers/Accueil',
      },
      {
        path: '/offres',
        component: 'src/containers/Offres',
      },
      {
        path: '/demo',
        component: 'src/containers/Demo',
        getProps: () => ({
          posts,
        }),
      },
      {
        path: '/devis',
        component: 'src/containers/Devis',
      },
      {
        path: '/contact',
        component: 'src/containers/Contact',
      },
      {
        is404: true,
        component: 'src/containers/Oups',
      },
    ]
  },
  Html: class CustomHtml extends Component {
    render() {
      const { Html, Head, Body, children } = this.props
      return (
        <Html lang="fr-FR">
          <Head>
            <meta charSet="utf-8" />
            <title>( blablagent )</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
            <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" />
            <link rel="stylesheet" href="/app.css" />
          </Head>
          <Body>
            {children}
          </Body>
        </Html>
      )
    }
  },
}
